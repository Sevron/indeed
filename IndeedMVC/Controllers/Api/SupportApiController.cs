﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IndeedMVC.Models;
using Microsoft.AspNetCore.Mvc;

namespace IndeedMVC.Controllers.Api
{
    [Route("api/[controller]")]
    public class SupportApiController : Controller
    {
        private IndeedSupportContext _supportContext;
        public SupportApiController(IndeedSupportContext supportContext)
        {
            _supportContext = supportContext;
        }

        // GET api/<controller>/5
        [HttpGet("{id}")]
        public ActionResult Status(int id)
        {
            bool? processedResult = false;
            processedResult = _supportContext.Ticket.Find(id)?.Processed ?? null;
            
            if (!processedResult.HasValue)
            {
                return NotFound(new JsonResult(new { message = "Cannot find ticket", result = string.Empty }));
            }

            return Ok(new JsonResult(new { message = "Ok", result = processedResult}));
        }

        // POST api/<controller>
        [HttpPost]
        public void Post([FromBody]Ticket value)
        {
            value.EnteredIn = DateTime.Now;
            //TODO нужна валидация
            _supportContext.Ticket.Add(value);
            _supportContext.SaveChanges();
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            //тикет еще не обработан
            var ticket = _supportContext.Ticket.FirstOrDefault(tk => tk.Id == id && !tk.Processed && tk.Executer == null);
            if (ticket != null)
            {
                ticket.Canceled = true;
                _supportContext.Ticket.Update(ticket);
                _supportContext.SaveChanges();
            }
        }
    }
}
