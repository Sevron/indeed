﻿using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using IndeedMVC.Models;
using IndeedMVC.Services.Utils;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace IndeedMVC.Controllers
{
    public class HomeController : Controller
    {
        private readonly IndeedSupportContext _supportContext;

        public HomeController(IndeedSupportContext supportContext)
        {
            _supportContext = supportContext;
        }

        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> SupportTeamIndex(string sortOrder, string currentFilter, string searchString, int? pageNumber)
        {
            ViewData["CurrentSort"] = sortOrder;
            ViewData["NameSortParm"] = string.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewData["DateSortParm"] = sortOrder == "Date" ? "date_desc" : "Date";

            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewData["CurrentFilter"] = searchString;

            var teams = from s in _supportContext.SupportTeam.Include(st => st.PositionNavigation)
                           select s;
            if (!string.IsNullOrEmpty(searchString))
            {
                teams = teams.Where(s => s.LastName.Contains(searchString)
                                       || s.FirstName.Contains(searchString));
            }
            switch (sortOrder)
            {
                case "name_desc":
                    teams = teams.OrderByDescending(s => s.LastName);
                    break;
                case "Date":
                    teams = teams.OrderBy(s => s.HireDate);
                    break;
                case "date_desc":
                    teams = teams.OrderByDescending(s => s.HireDate);
                    break;
                default:
                    teams = teams.OrderBy(s => s.LastName);
                    break;
            }

            int pageSize = 5;
            return View(await PaginatedList<SupportTeam>.CreateAsync(teams.AsNoTracking(), pageNumber ?? 1, pageSize));
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var teamMember = await _supportContext.SupportTeam
                 .Include(s => s.Ticket)
                 .AsNoTracking()
                 .FirstOrDefaultAsync(m => m.Id == id);

            if (teamMember == null)
            {
                return NotFound();
            }

            return View("CreateMember", teamMember);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(SupportTeam supportTeamMember)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _supportContext.Add(supportTeamMember);
                    await _supportContext.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
            }
            catch (DbUpdateException)
            {
                ModelState.AddModelError("", "Unable to save changes. It's not good.");
            }
            return View(_supportContext);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var teamMember = await _supportContext.SupportTeam.FindAsync(id);
            if (teamMember == null)
            {
                return NotFound();
            }
            ViewBag.Positions = new SelectList(_supportContext.SuppTeamPosition.Select(stp => new { Id = stp.Id, Name = stp.PositionName}), "Id", "Name");
            return View("EditMember", teamMember);
        }


        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditPost(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var supportTeamToUpdate = await _supportContext.SupportTeam.FirstOrDefaultAsync(s => s.Id == id);
            if (await TryUpdateModelAsync(
                supportTeamToUpdate,
                "",
                s => s.FirstName, s => s.LastName, s => s.HireDate, s => s.Position))
            {
                try
                {
                    await _supportContext.SaveChangesAsync();
                    return RedirectToAction(nameof(SupportTeamIndex));
                }
                catch (DbUpdateException)
                {
                    ModelState.AddModelError("", "Unable to save changes. It's not good.");
                }
            }
            return View(supportTeamToUpdate);
        }


        public async Task<IActionResult> Delete(int? id, bool? saveChangesError = false)
        {
            if (id == null)
            {
                return NotFound();
            }

            var teamMember = await _supportContext.SupportTeam
                .AsNoTracking()
                .FirstOrDefaultAsync(m => m.Id == id);
            if (teamMember == null)
            {
                return NotFound();
            }

            if (saveChangesError.GetValueOrDefault())
            {
                ViewData["ErrorMessage"] =
                    "Delete failed. It's not good.";
            }

            return View("DeleteMember", teamMember);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var student = await _supportContext.SupportTeam.FindAsync(id);
            if (student == null)
            {
                return RedirectToAction(nameof(Index));
            }

            try
            {
                _supportContext.SupportTeam.Remove(student);
                await _supportContext.SaveChangesAsync();
                return RedirectToAction(nameof(SupportTeamIndex));
            }
            catch (DbUpdateException)
            {
                return RedirectToAction(nameof(Delete), new { id = id, saveChangesError = true });
            }
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

    }
}
