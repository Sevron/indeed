﻿"use strict";

var connection = new signalR.HubConnectionBuilder().withUrl("/supportstatus").build();

////Disable send button until connection is established
//document.getElementById("sendButton").disabled = true;

connection.on("ShowSupportStatus", function (status) {
    console.log(status);
    document.getElementById("supportMonitoring").innerHTML = '';
    document.getElementById("supportMonitoring_Team").innerHTML = '';
    document.getElementById("supportMonitoring_TicketsHistory").innerHTML = '';

    var nonProcessedTickets = document.createElement("div");
    document.getElementById("supportMonitoring").appendChild(nonProcessedTickets);
    nonProcessedTickets.setAttribute("class", "col-md-3");
    nonProcessedTickets.innerText = "Requested(not in work) tickets count " + status.nonProcessedTickets;

    var inProcessTickets = document.createElement("div");
    document.getElementById("supportMonitoring").appendChild(inProcessTickets);
    inProcessTickets.setAttribute("class", "col-md-3");
    inProcessTickets.innerText = "Requested(in work) tickets count " + status.inProcessTickets;

    var processedTickets = document.createElement("div");
    document.getElementById("supportMonitoring").appendChild(processedTickets);
    processedTickets.setAttribute("class", "col-md-3");
    processedTickets.innerText = "Done tickets count " + status.processedTickets;

    var ticketHistory = document.createElement("div");
    document.getElementById("supportMonitoring").appendChild(ticketHistory);
    ticketHistory.setAttribute("class", "col-md-3");
    ticketHistory.innerText = "History of processed tickets count " + status.ticketHistoryCount;

    var teamDiv = document.getElementById("supportMonitoring_Team");
    var teamUL = document.createElement("ul");
    teamDiv.appendChild(teamUL);
    teamUL.setAttribute("class", "navi_members");

    for (const member of status.supportTeam) {
        var memberLi = document.createElement("li");
        memberLi.innerText = "Team member " + member.firstName + " " + member.lastName + " at position " + member.positionNavigation.positionName + " hired " + member.hireDate + "; Not busy " + member.freeStatus;
        teamUL.appendChild(memberLi);
    }

    var historyDiv = document.getElementById("supportMonitoring_TicketsHistory");
    var historyUl = document.createElement("ul");
    historyDiv.appendChild(historyUl);
    historyUl.setAttribute("class", "navi");
    for (const historyRecord of status.history) {
        var historyRecordLi = document.createElement("li");
        historyRecordLi.innerText = "Team member " + historyRecord.supportMember + " processing time " + historyRecord.processingTime + " seconds. Problem " + historyRecord.problemDescription + "; problem resolved at " + historyRecord.processedAt;
        historyUl.appendChild(historyRecordLi);
    }


});

connection.start().then(function () {
    console.log("Started")
}).catch(function (err) {
    return console.error(err.toString());
});

//document.getElementById("sendButton").addEventListener("click", function (event) {
//    var user = document.getElementById("userInput").value;
//    var message = document.getElementById("messageInput").value;
//    connection.invoke("SendMessage", user, message).catch(function (err) {
//        return console.error(err.toString());
//    });
//    event.preventDefault();
//});