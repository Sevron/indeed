﻿using IndeedMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IndeedMVC.Services.Interfaces
{
    public interface ISupportStatusHub
    {
        Task ShowSupportStatus(SupportStatusModel SupportStatus);
    }

}
