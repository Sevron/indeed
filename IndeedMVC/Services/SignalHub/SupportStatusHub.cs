﻿using IndeedMVC.Models;
using IndeedMVC.Services.Interfaces;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IndeedMVC.Services.Implementations
{
    public class SupportStatusHub : Hub<ISupportStatusHub>
    {
        public async Task ShowSupportStatus(SupportStatusModel SupportStatus)
        {
            await Clients.All.ShowSupportStatus(SupportStatus);
        }
    }
}
