﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IndeedMVC.Services.Utils
{
    public class TicketServiceOptions
    {
        public int MinExecuteTime { get; set; }

        public int MaxExecuteTime { get; set; }

        public int CheckUpdateTime { get; set; }

        public int SupportStatusUpdateDelay { get; set; }

        public int Td { get; set; }

        public int Tm { get; set; }
    }
}
