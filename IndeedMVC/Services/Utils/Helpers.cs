﻿using IndeedMVC.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;

namespace IndeedMVC.Services.Utils
{
    public static class Helpers
    {
        public static List<Ticket> DisturbOnTeamMembers(this List<Ticket> nonExecutingTickets, TeamPositions teamPositions, IndeedSupportContext context, IOptions<TicketServiceOptions> options)
        {
            if (nonExecutingTickets == null || nonExecutingTickets.Count <= 0)
            {
                return Enumerable.Empty<Ticket>().ToList();
            }
            var canDisturb = true;
            var currentTime = DateTime.Now;
            switch (teamPositions)
            {
                case TeamPositions.Operator:
                    canDisturb = true;
                    break;
                case TeamPositions.Manager:
                    canDisturb = nonExecutingTickets.Where(tk => (currentTime - tk.EnteredIn).TotalSeconds >= TimeSpan.FromSeconds(options.Value.Tm).TotalSeconds).ToList().Count > 0;
                    break;
                case TeamPositions.Director:
                    canDisturb = nonExecutingTickets.Where(tk => (currentTime - tk.EnteredIn).TotalSeconds >= TimeSpan.FromSeconds(options.Value.Td).TotalSeconds).ToList().Count > 0;
                    break;
                default:
                    break;
            }
            //распределяем по членам команды (если можем)
            var supportTeamsMembers = context.SupportTeam.Include(st => st.PositionNavigation).Where(st => st.FreeStatus
                                                            && st.PositionNavigation.PositionName.Equals(EnumItemAttribute.GetName(teamPositions))).ToList();
            if (supportTeamsMembers.Count > 0 && canDisturb)
            {
                var nowExecuted = nonExecutingTickets.Zip(supportTeamsMembers, (tk, supportMember) =>
                {
                    supportMember.FreeStatus = false;
                    supportMember.Ticket = tk;
                    context.SupportTeam.Update(supportMember);

                    tk.Executer = supportMember.Id;
                    tk.ProcessedStart = DateTime.Now;
                    context.Ticket.Update(tk);
                    return (operatorId: supportMember.Id, ticket: tk);
                }).ToList();
                nowExecuted.ForEach(a => nonExecutingTickets.Remove(a.ticket));
                context.SaveChanges();
            }
            return nonExecutingTickets;
        }
    }
}
