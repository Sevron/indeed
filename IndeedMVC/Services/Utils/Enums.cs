﻿using System;

namespace IndeedMVC.Services.Utils
{
    public abstract class AbstractAttribute : Attribute
    {
        protected static T GetEnumItemAttribute<T>(object enumItem)
            where T : new()
        {
            if (enumItem == null)
                throw new ArgumentNullException("enumItem");

            var fieldInfo = enumItem.GetType().GetField(enumItem.ToString());

            if (fieldInfo != null)
            {
                var fieldAttrs = fieldInfo.GetCustomAttributes(
                    typeof(T),
                    false
                );

                if (fieldAttrs != null && fieldAttrs.Length > 0)
                    return (T)fieldAttrs[0];
            }
            return new T();
        }

        protected static T GetAttribute<T>(object enumType)
            where T : new()
        {
            if (enumType == null)
                throw new ArgumentNullException("enumType");

            var attrs = ((Type)enumType).GetCustomAttributes(
                typeof(T),
                false
            );
            if (attrs != null && attrs.Length > 0)
                return (T)attrs[0];

            return new T();
        }

    }
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = false)]
    public class EnumItemAttribute : AbstractAttribute
    {
        #region Constructors
        public EnumItemAttribute()
        {
            Name = string.Empty;
        }
        #endregion

        #region Properties
        public string Name { get; set; }
        #endregion

        #region Static methods
        public static EnumItemAttribute GetEnumItemAttribute(object enumItem)
        {
            return GetEnumItemAttribute<EnumItemAttribute>(enumItem);
        }

        public static string GetName(object enumItem)
        {
            return GetEnumItemAttribute<EnumItemAttribute>(enumItem).Name;
        }

        #endregion
    }
    public enum TeamPositions
    {
        [EnumItem(Name = "Оператор")]
        Operator,
        [EnumItem(Name = "Менеджер")]
        Manager,
        [EnumItem(Name = "Директор")]
        Director,
    }
}
