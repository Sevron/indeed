﻿using IndeedMVC.Models;
using IndeedMVC.Services.Interfaces;
using IndeedMVC.Services.Utils;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace IndeedMVC.Services.Implementations
{
    public class SupportStatusService : BackgroundService
    {
        private readonly ILogger<SupportStatusService> _logger;
        private readonly IHubContext<SupportStatusHub, ISupportStatusHub> _supportStatusHub;
        private readonly IServiceScopeFactory _scopeFactory;
        private IOptions<TicketServiceOptions> _options;

        public SupportStatusService(ILogger<SupportStatusService> logger,
                                    IHubContext<SupportStatusHub, ISupportStatusHub> supportStatusHub,
                                    IServiceScopeFactory scopeFactory,
                                    IOptions<TicketServiceOptions> options)
        {
            _logger = logger;
            _supportStatusHub = supportStatusHub;
            _scopeFactory = scopeFactory;
            _options = options;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            using (var scope = _scopeFactory.CreateScope())
            {
                var _context = scope.ServiceProvider.GetRequiredService<IndeedSupportContext>();
                while (!stoppingToken.IsCancellationRequested)
                {
                    _logger.LogInformation("SupportStatusService running at: {Time}", DateTime.Now);
                    try
                    {
                        var supportStatus = new SupportStatusModel
                        {
                            //еще не в работе (не назначены)
                            NonProcessedTickets = _context.Ticket.Where(tk => !tk.Processed && tk.Executer == null)?.ToList().Count ?? 0,
                            //в работе (назначены)
                            InProcessTickets = _context.Ticket.Where(tk => !tk.Processed && tk.Executer != null)?.ToList().Count ?? 0,
                            //уже были обработаны (завершены)
                            ProcessedTickets = _context.Ticket.Where(tk => tk.Processed && tk.Executer == null)?.ToList().Count ?? 0,
                            TicketHistoryCount = _context.TicketHistory?.Count() ?? 0,
                            SupportTeam = _context.SupportTeam.Include(st => st.PositionNavigation)?.ToArray() ?? null,
                            History = _context.TicketHistory?.AsEnumerable().Select(tkh => new History()
                                                                                            {
                                                                                                ProcessingTime = tkh.ProcessingTime ?? 0,
                                                                                                SupportMember = _context.SupportTeam
                                                                                                                    .FirstOrDefault(tm => tm.Id == tkh.ProcessedBy)?
                                                                                                                        .GetFullName() ?? "Не известен",
                                                                                                ProblemDescription = _context.Ticket
                                                                                                                    .FirstOrDefault(tk => tk.Id == tkh.Ticket)?
                                                                                                                        .ProblemDescription ?? "Описание проблемы недоступно",
                                                                                                ProcessedAt = tkh.ProcessedAt
                                                                                            })?.ToArray() ?? null

                        };
                        await _supportStatusHub.Clients.All.ShowSupportStatus(supportStatus);
                    }
                    catch (Exception exc)
                    {
                        _logger.LogError(exc, "Errors when try collect status of support");
                    }
                    finally
                    {
                        await Task.Delay((int)TimeSpan.FromSeconds(_options.Value.SupportStatusUpdateDelay).TotalMilliseconds);
                    }
                }
            }
        }
    }
}
