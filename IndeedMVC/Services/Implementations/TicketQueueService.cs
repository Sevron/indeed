﻿using IndeedMVC.Models;
using System;
using System.Collections.Concurrent;

namespace IndeedMVC.Services.Implementations
{
    public class TicketQueueService : ITicketQueueService
    {
        private ConcurrentQueue<Ticket> tasksQueue;

        public TicketQueueService()
        {
            tasksQueue = new ConcurrentQueue<Ticket>();
        }

        public void AddNewTicket(Ticket ticket)
        {
            tasksQueue.Enqueue(ticket);
        }

        public Ticket GetTicketForWork()
        {
            var isSuccessTrying = tasksQueue.TryDequeue(out Ticket ticket);
            
            if (isSuccessTrying)
                return ticket;

            return null;
        }

        public bool IsEmpty()
        {
            return tasksQueue.IsEmpty;
        }
    }
}
