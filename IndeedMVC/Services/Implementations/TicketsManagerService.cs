﻿using IndeedMVC.Models;
using IndeedMVC.Services.Utils;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace IndeedMVC.Services.Implementations
{
    public class TicketsManagerService : BackgroundService
    {
        private IOptions<TicketServiceOptions> _options;
        private ILogger<TicketsManagerService> _logger;
        private IServiceScopeFactory _scopeFactory;
        private TicketQueueService _ticketQueue;
        public TicketsManagerService(IOptions<TicketServiceOptions> options,
                                     ILogger<TicketsManagerService> logger,
                                     IServiceScopeFactory scopeFactory,
                                     TicketQueueService ticketQueueService)
        {
            _options = options;
            _logger = logger;
            _scopeFactory = scopeFactory;
            _ticketQueue = ticketQueueService;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _logger.LogDebug($"TicketsManagerService is starting.");

            stoppingToken.Register(() =>
                _logger.LogDebug($"TicketsManagerService task is stopping."));

            while (!stoppingToken.IsCancellationRequested)
            {
                _logger.LogDebug($"TicketsManagerService doing background work.");

                TicketDistribution();
                await Task.Delay((int)TimeSpan.FromSeconds(_options.Value.CheckUpdateTime).TotalMilliseconds, stoppingToken);
            }

            _logger.LogDebug($"TicketsManagerService task is stopping.");
            await Task.CompletedTask;
        }

        private void TicketDistribution()
        {
            _logger.LogDebug($"Checking queue of tickets grace period orders");
            //Освобождаем работников от выполненных задач (пройдено время)
            SaveExecutedTickets();

            //если очередь тикетов не пуста
            if (!_ticketQueue.IsEmpty())
            {
                //сначала идем к ней
                DisturbTicketsFromQueue();
            }
            else
            {
                DisturbTicketsFromDB();
            }
        }

        private bool SaveExecutedTickets()
        {
            using (var scope = _scopeFactory.CreateScope())
            {
                var _context = scope.ServiceProvider.GetRequiredService<IndeedSupportContext>();
                var succeedSave = true;

                var now = DateTime.Now;
                //задачи еще не выполненные, но с назначенным исполнителем (то есть выполняемые) и временем уже большим чем минимальное время исполнения задач
                //и если мы тут вылетим с null reff (то есть задача с исполнителем, но без даты начала работ) то у нас определенно есть проблемы
                var executedTickets = _context.Ticket.Where(tk => tk.Processed == false && tk.Executer.HasValue && (now - tk.ProcessedStart.Value).TotalSeconds >= TimeSpan.FromSeconds(_options.Value.MinExecuteTime).TotalSeconds).Include(tk => tk.ExecuterNavigation).ToList();

                foreach (var ticket in executedTickets)
                {
                    var teamMember = ticket.ExecuterNavigation;
                    //освобождаем
                    teamMember.FreeStatus = true;
                    //запись в таблицу истории
                    var ticketSaveNow = DateTime.Now;
                    TicketHistory ticketHistory = new TicketHistory()
                    {
                        EnteredIn = ticket.EnteredIn,
                        ProcessedAt = ticketSaveNow,
                        ProcessedStart = ticket.ProcessedStart,
                        ProcessedBy = teamMember.Id,
                        ProcessingTime = (int)(ticketSaveNow - ticket.ProcessedStart.Value).TotalSeconds,
                        Ticket = ticket.Id
                    };
                    _context.TicketHistory.Add(ticketHistory);
                    //завершение задачи
                    ticket.Executer = null;
                    ticket.Processed = true;
                    _context.SaveChanges();
                }
                return succeedSave;
            }
        }

        private void DisturbTicketsFromDB()
        {
            using (var scope = _scopeFactory.CreateScope())
            {
                var _context = scope.ServiceProvider.GetRequiredService<IndeedSupportContext>();
                //получаем задачи из базы, которые надо исполнить, где самые долгоожидающие (давно прибывшие) должны обработаться первыми
                //кроме PK индексов в базе нет, проблема догонит с увеличением размера бд
                var nonExecutingTickets = _context.Ticket.Where(tk => !tk.Processed && tk.Executer == null).OrderByDescending(tk => tk.EnteredIn).ToList();
                DistrubTickets(nonExecutingTickets, _context);
            }
        }
        private void DisturbTicketsFromQueue()
        {
            using (var scope = _scopeFactory.CreateScope())
            {
                var _context = scope.ServiceProvider.GetRequiredService<IndeedSupportContext>();
                var tickets = new List<Ticket>();
                while (!_ticketQueue.IsEmpty())
                {
                    tickets.Add(_ticketQueue.GetTicketForWork());
                }
                DistrubTickets(tickets, _context);
            }
        }
        private void DistrubTickets(List<Ticket> nonExecutingTickets, IndeedSupportContext _context)
        {
            //все операторы получают задачи, если у задач время больше чем Tm то получают свободные менеджеры, если у задач время больше чем Td то Директор
            if (nonExecutingTickets.Count <= 0)
            {
                return;
            }
            try
            {
                //распределяем пока можем
                nonExecutingTickets = nonExecutingTickets.DisturbOnTeamMembers(TeamPositions.Operator, _context, _options)
                                                            .DisturbOnTeamMembers(TeamPositions.Manager, _context, _options)
                                                            .DisturbOnTeamMembers(TeamPositions.Director, _context, _options);
                //очередь никак не ограничена
                nonExecutingTickets.ForEach(tk => _ticketQueue.AddNewTicket(tk));
            } catch (Exception exc)
            {
                _logger.LogError(exc, "troubles");
            }
        }
    }
}
