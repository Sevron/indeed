﻿using IndeedMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IndeedMVC.Services
{
    public interface ITicketQueueService
    {
        //запрос на тех поддержку который не может быть тут же обработан попадает в очередь
        void AddNewTicket(Ticket guid);
        //проверка что очередь запросов пуста
        bool IsEmpty();
        //получить запрос из очереди на обработку сотрудником
        Ticket GetTicketForWork();

    }
}
