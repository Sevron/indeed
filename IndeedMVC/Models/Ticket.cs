﻿using System;
using System.Collections.Generic;

namespace IndeedMVC.Models
{
    public partial class Ticket
    {
        public int Id { get; set; }
        public DateTime EnteredIn { get; set; }
        public DateTime? ProcessedStart { get; set; }
        public string ProblemDescription { get; set; }
        public bool Processed { get; set; }
        public int? Executer { get; set; }
        public bool Canceled { get; set; }

        public SupportTeam ExecuterNavigation { get; set; }
    }
}
