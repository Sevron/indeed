﻿using System;
using System.Collections.Generic;

namespace IndeedMVC.Models
{
    public partial class SuppTeamPosition
    {
        public SuppTeamPosition()
        {
            SupportTeam = new HashSet<SupportTeam>();
        }

        public int Id { get; set; }
        public string PositionName { get; set; }

        public ICollection<SupportTeam> SupportTeam { get; set; }
    }
}
