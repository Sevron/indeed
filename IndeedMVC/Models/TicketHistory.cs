﻿using System;
using System.Collections.Generic;

namespace IndeedMVC.Models
{
    public partial class TicketHistory
    {
        public int Id { get; set; }
        public DateTime EnteredIn { get; set; }
        public DateTime? ProcessedStart { get; set; }
        public DateTime ProcessedAt { get; set; }
        public int ProcessedBy { get; set; }
        public int? ProcessingTime { get; set; }
        public int Ticket { get; set; }
    }
}
