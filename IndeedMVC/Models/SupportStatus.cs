﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IndeedMVC.Models
{
    public struct History
    {
        public string SupportMember { get; set; }
        public int ProcessingTime { get; set; }
        public string ProblemDescription { get; set; }

        public DateTime ProcessedAt { get; set; }
    }
    public class SupportStatusModel
    {
        //необработанных
        public int NonProcessedTickets { get; set; }
        //обработанных
        public int ProcessedTickets { get; set; }
        //обрабатываемых
        public int InProcessTickets { get; set; }
        //история тикетов
        //всего
        public int TicketHistoryCount { get; set; }
        //кем обработан и за какое время
        public History[] History { get; set; }
        public SupportTeam[] SupportTeam { get; set; }
    }
}
