﻿using System;
using System.Collections.Generic;

namespace IndeedMVC.Models
{
    public partial class SupportTeam
    {
        public int Id { get; set; }
        public DateTime? HireDate { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public bool FreeStatus { get; set; }
        public int Position { get; set; }

        public SuppTeamPosition PositionNavigation { get; set; }
        public Ticket Ticket { get; set; }
    }
}
