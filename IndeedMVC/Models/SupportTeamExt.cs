﻿namespace IndeedMVC.Models
{
    public partial class SupportTeam
    {
        public string GetFullName()
        {
            return $"{(PositionNavigation != null ? PositionNavigation.PositionName : string.Empty)} {FirstName} {LastName}";
        }
    }
}
