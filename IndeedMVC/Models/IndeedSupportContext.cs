﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace IndeedMVC.Models
{
    public partial class IndeedSupportContext : DbContext
    {
        public IndeedSupportContext()
        {
        }

        public IndeedSupportContext(DbContextOptions<IndeedSupportContext> options)
            : base(options)
        {
        }

        public virtual DbSet<SupportTeam> SupportTeam { get; set; }
        public virtual DbSet<SuppTeamPosition> SuppTeamPosition { get; set; }
        public virtual DbSet<Ticket> Ticket { get; set; }
        public virtual DbSet<TicketHistory> TicketHistory { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {

            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<SupportTeam>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.FirstName)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.FreeStatus)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.HireDate)
                    .HasColumnType("datetime2(2)")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.LastName)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.HasOne(d => d.PositionNavigation)
                    .WithMany(p => p.SupportTeam)
                    .HasForeignKey(d => d.Position)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_Position");
            });

            modelBuilder.Entity<SuppTeamPosition>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.PositionName)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('Не указано')");
            });

            modelBuilder.Entity<Ticket>(entity =>
            {
                entity.HasIndex(e => e.Executer)
                    .HasName("idx_executer_notnull")
                    .IsUnique()
                    .HasFilter("([Executer] IS NOT NULL)");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.EnteredIn)
                    .HasColumnType("datetime2(2)")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ProblemDescription)
                    .IsRequired()
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.ProcessedStart).HasColumnType("datetime2(2)");

                entity.HasOne(d => d.ExecuterNavigation)
                    .WithOne(p => p.Ticket)
                    .HasForeignKey<Ticket>(d => d.Executer)
                    .HasConstraintName("fk_Ticket_Executer");
            });

            modelBuilder.Entity<TicketHistory>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.EnteredIn).HasColumnType("datetime2(2)");

                entity.Property(e => e.ProcessedAt).HasColumnType("datetime2(2)");

                entity.Property(e => e.ProcessedStart).HasColumnType("datetime2(2)");

                entity.Property(e => e.ProcessingTime).HasDefaultValueSql("((0))");
            });
        }
    }
}
