﻿using IndeedMVC.Models;
using IndeedMVC.Services.Implementations;
using IndeedMVC.Services.Utils;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace IndeedMVC
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        //если это задача на пару вечеров, то у меня проблемы, so/msdn very grateful
        public void ConfigureServices(IServiceCollection services)
        {
            //контекст с минимальным временем жизни
            services.AddDbContext<IndeedSupportContext>(options =>
                        options.UseSqlServer(Configuration.GetConnectionString("SupportDatabase")), ServiceLifetime.Transient);
            //опции приложения для саппорта
            services.Configure<TicketServiceOptions>(Configuration.GetSection("SupportOptions"));
            services.Configure<CookiePolicyOptions>(options =>
            {
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });
            //очередь которая держит в себе тикеты, которые надо разобрать в первоочередном порядке
            services.AddSingleton<TicketQueueService>();
            //сервис бэка выдающий текущее состояние саппорта
            services.AddHostedService<SupportStatusService>();
            //сервис бэка наблюдающий за тикетами и распределяющий их
            services.AddHostedService<TicketsManagerService>();
            //старый добрый, опция для сериализатора
            services.AddSignalR().AddJsonProtocol(options =>
            {
                options.PayloadSerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
            });

            //тут тоже страхуемся
            services.AddMvc().AddJsonOptions(options => 
                                                { options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore; })
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
            //концентратор
            app.UseSignalR(routes =>
            {
                routes.MapHub<SupportStatusHub>("/supportstatus");
            });
        }
    }
}
