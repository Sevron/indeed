﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SupportGenerateLoadTest
{
    public static class Extensions
    {
        //ключ параметра и его значение это два слова, например -m input или -f C:\Temp
        private static int ArgsKeyValueNumWords = 2;
        public static Dictionary<string, string> ParseArgs(string[] args)
        {
            var result = new Dictionary<string, string>();
            //каждый параметр настройки должен иметь значение
            if (args.Length % ArgsKeyValueNumWords != 0)
            {
                throw new ArgumentException("Incorrect params in application start");
            }
            //режем на куски по два слова аргументы запуска, 
            //затем подтягиваем до фактического расчета, и переводим в словарь, где ключ первая часть, а значение вторая
            //получается что массив строк преобразуется в словарь
            result = args.ToList().Split(ArgsKeyValueNumWords).ToList().Select(a => a.ToList()).ToDictionary(k => k[0], v => v[1]);
            return result;
        }

        public static IEnumerable<IEnumerable<T>> Split<T>(this IEnumerable<T> source, int size)
        {
            var i = 0;
            return
                from element in source
                group element by i++ / size into splitGroups
                select splitGroups.AsEnumerable();
        }
    }
}
