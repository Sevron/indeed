﻿using NLog;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;

namespace SupportGenerateLoadTest
{
    public class Ticket
    {
        public int Id { get; set; }
        public DateTime EnteredIn { get; set; }
        public string ProblemDescription { get; set; }
        public bool Processed { get; set; }
        public int? Executer { get; set; }
        public bool Canceled { get; set; }

        
    }

    class Program
    {

        private static Logger nLog = LogManager.GetCurrentClassLogger();

        private const int DEFAULT_QUERIES_COUNT = 100;
        static HttpClient client = new HttpClient();
        static void Main(string[] args)
        {
            nLog.Debug(args.ToList().Aggregate(string.Empty, (a, b) => a + b));
            var parsedArgs = Extensions.ParseArgs(args);
            try
            {
                var minTimeBetweenRequestSeconds = int.Parse(parsedArgs["-minTimeDelay"]);
                var maxTimeBetweenRequestSeconds = int.Parse(parsedArgs["-maxTimeDelay"]);
                var address = parsedArgs["-url"];
                var queriesCount = 0;
                if (parsedArgs.Keys.Contains("-reqCount"))
                {
                    queriesCount = int.Parse(parsedArgs["-reqCount"]);
                }
                Validate(maxTimeBetweenRequestSeconds, minTimeBetweenRequestSeconds, address, queriesCount);
                RunAsync(address, maxTimeBetweenRequestSeconds, minTimeBetweenRequestSeconds, queriesCount).GetAwaiter().GetResult();
            } catch (Exception exc)
            {
                Console.WriteLine(exc.Message);
                nLog.Error(exc);
            }
            
        }

        private static void Validate(int maxTimeBetweenRequestSeconds, int minTimeBetweenRequestSeconds, string address, int queriesCount)
        {
            if(maxTimeBetweenRequestSeconds <= 0 || minTimeBetweenRequestSeconds <= 0)
            {
                throw new ArgumentException("Max or min time between request incorrect");
            }
            if (string.IsNullOrEmpty(address))
            {
                throw new ArgumentException("Url is empty");
            }
            if (minTimeBetweenRequestSeconds > maxTimeBetweenRequestSeconds)
            {
                minTimeBetweenRequestSeconds = maxTimeBetweenRequestSeconds;
            }
            if (queriesCount < 0)
            {
                queriesCount = DEFAULT_QUERIES_COUNT;
            }
        }

        static async Task RunAsync(string url, int maxTime, int minTime, int queryCount)
        {
            // Update port # in the following line.
            client.BaseAddress = new Uri(url);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));

            try
            {
                var random = new Random(DateTime.Now.GetHashCode());
                var needWork = true;
                var generatedQueriesCount = 0;
                while (needWork)
                {
                    //ждем
                    Thread.Sleep((int)TimeSpan.FromSeconds(random.Next(minTime, maxTime)).TotalMilliseconds);
                    // Create a new ticket
                    Ticket ticket = new Ticket
                    {
                        ProblemDescription = $"Trouble in whatever {random.Next()} and it was {Guid.NewGuid()};",
                        Canceled = false,
                        Processed = false
                    };
                    try
                    {
                        var statusCode = await CreateTicketAsync(ticket);
                    } catch (Exception exc)
                    {
                        Console.WriteLine($"Some troubles in query to api {exc.Message}");
                    }
                    generatedQueriesCount++;
                    //выйти мы можем только в том случае если queryCount не был задан <=0 и когда мы >= (если queryCount был задан положительным числом)
                    //за максимальное кол-во запросов
                    if (queryCount > 0 && generatedQueriesCount >= queryCount)
                    {
                        needWork = false;
                    }
                }
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc.Message);
                nLog.Error(exc);
            }

            Console.ReadLine();
        }
        static async Task<HttpStatusCode> CreateTicketAsync(Ticket ticket)
        {
            HttpResponseMessage response = await client.PostAsJsonAsync(
                "api/supportapi", ticket);
            return response.StatusCode;
        }
    }
}
