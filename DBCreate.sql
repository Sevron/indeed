USE [master]
GO
IF NOT EXISTS 
   (SELECT name FROM master.dbo.sysdatabases WHERE name = N'IndeedSupport')
BEGIN
	CREATE DATABASE [IndeedSupport]
	
	ALTER DATABASE [IndeedSupport] SET COMPATIBILITY_LEVEL = 120
	
END
GO

/****** Object:  Database [IndeedSupport]    Script Date: 17.11.2019 21:45:38 ******/
If not Exists (select loginname from master.dbo.syslogins 
    where name = 'web_mvc_support')
Begin
	CREATE LOGIN [web_mvc_support] WITH PASSWORD = 'helloworld'
End

USE [IndeedSupport]
GO
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'web_mvc_support')
BEGIN
    CREATE USER [web_mvc_support] FOR LOGIN [web_mvc_support]
    EXEC sp_addrolemember N'db_owner', N'web_mvc_support'
END;
GO

IF (OBJECT_ID('dbo.fk_Position', 'F') IS NOT NULL)
    ALTER TABLE [dbo].[SupportTeam] DROP CONSTRAINT fk_Position
GO
IF (OBJECT_ID('dbo.fk_Ticket_Executer', 'F') IS NOT NULL)
    ALTER TABLE [dbo].[Ticket] DROP CONSTRAINT fk_Ticket_Executer
GO

if object_id('[dbo].[SupportTeam]', 'U') IS NOT NULL
    drop table [dbo].[SupportTeam];
GO
 
if object_id('[dbo].[SuppTeamPosition]', 'U') IS NOT NULL
    drop table [dbo].[SuppTeamPosition];
GO

if object_id('[dbo].[Ticket]', 'U') IS NOT NULL
    drop table [dbo].[Ticket];
GO

if object_id('[dbo].[TicketHistory]', 'U') IS NOT NULL
    drop table [dbo].[TicketHistory];
GO

CREATE TABLE [dbo].[SupportTeam] (
    ID int IDENTITY(1,1) NOT NULL,
	HireDate datetime2(2),
    LastName varchar(255),
    FirstName varchar(255),
	FreeStatus bit NOT NULL,
	Position int NOT NULL,
	CONSTRAINT [pk_st_ID] PRIMARY KEY CLUSTERED (
		[ID] ASC
	)
);
ALTER TABLE [dbo].[SupportTeam] ADD  CONSTRAINT [df_st_hd]  DEFAULT (getdate()) FOR [HireDate]
GO
ALTER TABLE [dbo].[SupportTeam] ADD  CONSTRAINT [df_st_fs]  DEFAULT (1) FOR [FreeStatus]
GO

CREATE TABLE [dbo].[SuppTeamPosition] (
    ID int IDENTITY(1,1) NOT NULL,
    PositionName varchar(255) NOT NULL,
    CONSTRAINT [pk_stp_ID] PRIMARY KEY CLUSTERED (
		[ID] ASC
	)
);
ALTER TABLE [dbo].[SuppTeamPosition] ADD  CONSTRAINT [df_stp_pn]  DEFAULT ('Не указано') FOR [PositionName]
GO
ALTER TABLE [dbo].[SupportTeam] ADD CONSTRAINT [fk_Position] FOREIGN KEY (Position) references SuppTeamPosition(ID) ON DELETE NO ACTION ON UPDATE CASCADE
GO


INSERT INTO [dbo].[SuppTeamPosition]
           ([PositionName])
     VALUES
           ('Оператор');
INSERT INTO [dbo].[SuppTeamPosition]
           ([PositionName])
     VALUES
           ('Менеджер');
INSERT INTO [dbo].[SuppTeamPosition]
           ([PositionName])
     VALUES
           ('Директор')
GO

USE [IndeedSupport]
GO

INSERT INTO [dbo].[SupportTeam]
           ([HireDate]
           ,[LastName]
           ,[FirstName]
           ,[Position])
     VALUES
           (GETDATE()
           ,'Васильев'
           ,'Вася'
           ,1)
GO
INSERT INTO [dbo].[SupportTeam]
           ([HireDate]
           ,[LastName]
           ,[FirstName]
           ,[Position])
     VALUES
           (GETDATE()
           ,'Румянов'
           ,'Петя'
           ,2)
GO
INSERT INTO [dbo].[SupportTeam]
           ([HireDate]
           ,[LastName]
           ,[FirstName]
           ,[Position])
     VALUES
           (GETDATE()
           ,'Директоров'
           ,'Леша'
           ,3)
GO


CREATE TABLE [dbo].[Ticket] (
    ID int IDENTITY(1,1) NOT NULL,
	EnteredIn datetime2(2) NOT NULL,
	ProcessedStart datetime2(2) NULL,
	ProblemDescription varchar(500) NOT NULL, 
	Processed bit NOT NULL,
	Executer int NULL,
	Canceled bit NOT NULL,
    CONSTRAINT [pk_t_ID] PRIMARY KEY CLUSTERED (
		[ID] ASC
	)
);
ALTER TABLE [dbo].[Ticket] ADD CONSTRAINT [df_t_ei]  DEFAULT (getdate()) FOR [EnteredIn]
GO
ALTER TABLE [dbo].[Ticket] ADD CONSTRAINT [df_t_p]  DEFAULT (0) FOR [Processed]
GO
--OneToOne отношение, иначе у исполнителя текущая задача станет коллекцией что неудобно, а просто unique не работает
-- так как у выполненных задач исполнитель null и начинается ругань
CREATE UNIQUE NONCLUSTERED INDEX idx_executer_notnull
ON [dbo].[Ticket](Executer) WHERE Executer IS NOT NULL;

ALTER TABLE [dbo].[Ticket] ADD CONSTRAINT [fk_Ticket_Executer] FOREIGN KEY (Executer) REFERENCES [dbo].[SupportTeam]([ID])
GO

CREATE TABLE [dbo].[TicketHistory] (
    ID int IDENTITY(1,1) NOT NULL,
	EnteredIn datetime2(2) NOT NULL,
	ProcessedStart datetime2(2) NULL,
	ProcessedAt datetime2(2) NOT NULL, 
    ProcessedBy int NOT NULL,
    ProcessingTime int,
	Ticket int NOT NULL,
	CONSTRAINT [pk_th_ID] PRIMARY KEY CLUSTERED (
		[ID] ASC
	)
);
ALTER TABLE [dbo].[TicketHistory] ADD  CONSTRAINT [df_th_pt]  DEFAULT (0) FOR [ProcessingTime]
GO
